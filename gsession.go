package gsession

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"net"
	"container/list"
	"time"
	"sync"
)

var ttl int64 = 600 // Session time to live: 10 min

//SetTTL sets session time to live in seconds
func SetTTL(newTTL int64) {
	ttl = newTTL
}

//Storage is an interface for session details storage
type Storage interface {
	Initialize()                        // initialize storage
	Truncate()                          // delete all storage contents
	Load(string) (*GoAPISession, error) // load session from Storage by session ID
	save(GoAPISession) error            // save session
	deleteSession(string)               // delete session
	CollectGarbage()                    // delete timed-out sessions
}

//NewMemoryStorage creates new memory storage
func NewMemoryStorage() *MemoryStorage {
	mStorage := MemoryStorage{list: list.New()}
	go mStorage.CollectGarbage()

	return &mStorage
}

//MemoryStorage is an in-memory implementation of Storage interface
type MemoryStorage struct {
	lock sync.Mutex
	s    map[string]*list.Element // data storage
	list *list.List               // GC
}

//Initialize initializaes in-memory storage
func (m *MemoryStorage) Initialize() {
	m.lock.Lock()
	defer m.lock.Unlock()

	m.s = make(map[string]*list.Element)
	m.list.Init()
}

//Truncate flushes session storage
func (m *MemoryStorage) Truncate() {
	m.Initialize()
}

//Load loads session from the storage
func (m *MemoryStorage) Load(ID string) (g *GoAPISession, err error) {
	m.lock.Lock()
	defer m.lock.Unlock()

	if element, ok := m.s[ID]; !ok {
		err = errors.New("Session not found:" + ID)
	} else {
		g = element.Value.(*GoAPISession)
		g.expire = time.Now().Unix() + ttl
        	m.list.MoveToBack(element)
	}

	return
}

//save saves session to the storage
func (m *MemoryStorage) save(s GoAPISession) error {
	m.lock.Lock()
	defer m.lock.Unlock()

	var err error
	if s.ID == "" {
		err = errors.New("Cannot save empty session struct")
	} else {
		element := m.list.PushBack(&s)
		m.s[s.ID] = element
	}
	return err
}

//deleteSession deletes session by ID
func (m *MemoryStorage) deleteSession(ID string) {
	m.lock.Lock()
	defer m.lock.Unlock()

	if element, ok := m.s[ID]; ok {
		delete(m.s, ID)
		m.list.Remove(element)
	}
}

//CollectGarbage deletes outdated sessions
func (m *MemoryStorage) CollectGarbage() {
	sleepTime := ttl
	for {
		element := m.list.Front()
		if element == nil {
			break
		}

		if (element.Value.(*GoAPISession).expire) <= time.Now().Unix() {
			m.deleteSession(element.Value.(*GoAPISession).ID)
		} else {
			sleepTime = element.Value.(*GoAPISession).expire - time.Now().Unix()
			break
		}
	}

	time.AfterFunc(time.Duration(sleepTime * 1000000000), func() { m.CollectGarbage() })
}

//GoAPISession is a struct to store session details
type GoAPISession struct {
	// Session ID, used to load/store session
	ID string
	//session expire - unix timestamp, server time checked
	expire int64
	//IP is a session owner IP
	IP net.IP
	//Data is a session data
	Data map[string]interface{}
	//Storage where this session is stored
	st Storage
}

// GenerateRandomBytes returns securely generated random bytes.
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomString returns a URL-safe, base64 encoded securely generated random string.
func GenerateRandomString(s int) (string, error) {
	b, err := GenerateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

//NewSession returns new session
func NewSession(storage Storage) (GoAPISession, error) {
	var err error
	g := GoAPISession{}
	g.ID, err = GenerateRandomString(64)
	g.Data = make(map[string]interface{})
	g.expire = time.Now().Unix() + ttl
	g.st = storage
	return g, err
}

//Save is a session wrapper for storage.save
func (g GoAPISession) Save() error {
	err := g.st.save(g)
	return err
}

//Delete is a session wrapper for storage.Delete
func (g GoAPISession) Delete() {
	g.st.deleteSession(g.ID)
}
