package gsession

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"os"
	"time"
)

var st Storage

func TestMain(m *testing.M) {
	SetTTL(3)
	st = NewMemoryStorage()
	os.Exit(m.Run())
}

func TestInterfaceWorksOk(t *testing.T) {
	assert.NotNil(t, st)
}

func TestCreateAndSaveInmemorySession(t *testing.T) {
	st.Initialize()
	session, err := NewSession(st)
	assert.Nil(t, err)
	assert.NotEqual(t, session.ID, "")
	err = session.Save()
	assert.Nil(t, err)
}

func TestSessionsHaveDifferentIDs(t *testing.T) {
	st.Initialize()
	session1, _ := NewSession(st)
	session2, _ := NewSession(st)
	session3, _ := NewSession(st)
	assert.NotEqual(t, session1.ID, session2.ID)
	assert.NotEqual(t, session1.ID, session3.ID)
	assert.NotEqual(t, session3.ID, session2.ID)
}

func TestStoreValueInSession(t *testing.T) {
	st.Initialize()
	session, err := NewSession(st)
	session.Data["123"] = "456"
	session.Save()
	session2, err := st.Load(session.ID)
	assert.Nil(t, err)
	assert.EqualValues(t, &session, session2)
	assert.Equal(t, session.ID, session2.ID)
}

func TestDeleteSession(t *testing.T) {
	st.Initialize()
	session, err := NewSession(st)
	session.Data["123"] = "456"
	session.Save()
	session2, err := st.Load(session.ID)
	st.deleteSession(session2.ID)
	session3, err := st.Load(session2.ID)
	assert.NotNil(t, err)
	assert.Nil(t, session3)
}

func TestSaveWrapperForSession(t *testing.T) {
	st.Initialize()
	session, _ := NewSession(st)
	session.Data["zzz"] = 518
	err := session.Save()
	assert.Nil(t, err)
}

func TestDeleteWrapperForSession(t *testing.T) {
	st.Initialize()
	session, err := NewSession(st)
	session.Data["123"] = "456"
	session.Save()
	session2, err := st.Load(session.ID)
	ID := session2.ID
	session2.Delete()
	session3, err := st.Load(ID)
	assert.NotNil(t, err)
	assert.Nil(t, session3)
}

func TestCollectGarbage(t *testing.T) {
	st.Initialize()

	session1, _ := NewSession(st)
	session1.Data["123"] = "456"
	session1.Save()

	session2, _ := NewSession(st)
	session2.Data["123"] = "789"
	session2.Save()

	_, err1 := st.Load(session1.ID)
	_, err2 := st.Load(session2.ID)
	assert.Nil(t, err1)
	assert.Nil(t, err2)

	// after 2 seconds extend session 1 life time
	time.Sleep(2 * time.Second)
	_, err3 := st.Load(session1.ID)
	assert.Nil(t, err3)

	// after more 2 seconds (4 seconds from start) session 2 must be expired, but session 1 - not (this extends session1 life time, too)
	time.Sleep(2 * time.Second)
	_, err4 := st.Load(session1.ID)
	assert.Nil(t, err4)
	_, err5 := st.Load(session2.ID)
	assert.NotNil(t, err5)

	// after more 4 seconds (8 seconds from start, 4 seconds from last session1 life time extension) session 1 must be expired, too
	time.Sleep(4 * time.Second)
	_, err6 := st.Load(session1.ID)
	assert.NotNil(t, err6)
}
